var express = require('express');
var serveStatic = require('serve-static');
var path = require('path');
var fs = require('fs');
var save = require('./daily-save');

var app = express();

// Get certificates for free here
// https://letsencrypt.org/
/*
var key = fs.readFileSync('./key.pem');
var cert = fs.readFileSync('./cert.pem')
*/

var options = {
  index: 'velo-toulouse-stats.html'
//    key: key,
//    cert: cert
};

const scriptPath = './public/scripts';

// Start all polling scripts
fs.readdir(scriptPath, function(err, files){
  if (err) return console.log(err);

  for(var i = 0 ; i < files.length ; i++) {
    console.log('Starting ' + files[i])
    require(path.join(__dirname, scriptPath, files[i]));
  }
});

app.use('/', serveStatic(path.join(__dirname,'public','viz'), options));
app.use('/data', serveStatic(path.join(__dirname,'public','data')));
app.listen(80);

console.log('Serving files.');
