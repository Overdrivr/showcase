var schedule = require('node-schedule');
var fs = require('fs');
var path = require('path');
var s3 = require('s3');
var AWS = require('aws-sdk');

var awsS3Client = new AWS.S3({
  region: "eu-central-1",
  signature: 'v4',
});

var client = s3.createClient({
  s3Client: awsS3Client
});

var directory = './public/data/';

var rule = new schedule.RecurrenceRule();
rule.hour = 2;
rule.minute = 0;

var j = schedule.scheduleJob(rule, function(){
  fs.readdir(directory, function(err, files){
    if (err) return console.log(err);
    files.forEach(function(file){
      uploadToS3(directory, file);
    });
  });
});

var uploadToS3 = function(directory, filename) {
  var params = {
    localFile: path.join(directory, filename),

    s3Params: {
      Bucket: "remibeges-website-data",
      Key: "VeloToulousePolling/" + filename,
    },
  };

  var uploader = client.uploadFile(params);

  console.log("Uploading %s", filename);

  uploader.on('error', function(err) {
    console.error("unable to upload:", err.stack);
  });

  uploader.on('progress', function() {
    console.log("progress", uploader.progressMd5Amount,
              uploader.progressAmount, uploader.progressTotal);
  });

  uploader.on('end', function() {
    console.log("done uploading");
  });
}
