var request = require('request');
var fs = require('fs');
var process = require('process');

var totalStatsFile = "./public/data/polling_velo_toulouse.csv";
var totalStatsHeaders = "unixtime,available_bikes,available_stands,total_stands\n";

var stationsStatsFile = "./public/data/polling_velo_toulouse_stations.csv";
var stationsStatsHeaders = "unixtime,empty_stations,full_stations\n";


var appkey = process.argv[2] || process.env.JCDecauxKey;
var frequency = process.argv[3] || 60;

console.log("Passed app key : " + appkey);
console.log("Polling every  : " + frequency + "(s)");

pollApi();

function pollApi() {
  setTimeout(pollApi, 1000 * frequency);
  console.log("Polling.");

  // API request
  request('https://api.jcdecaux.com/vls/v1/stations?contract=Toulouse&apiKey=' + appkey,
  function(err, res, body){
    if (err) return console.log(err);
    body = JSON.parse(body);

    if(res.statusCode != 200) return console.log(res.body);

    /* Initialize all metrics */
    var avail_bikes = 0;
    var avail_stands = 0;
    var total_stands = 0;
    var update_time = Date.now();
    var full_stations = 0;
    var empty_stations = 0;

    console.log("Polled now : " + update_time);

    for(var i = 0 ; i < body.length ; i++) {
      avail_bikes += body[i].available_bikes;
      avail_stands += body[i].available_bike_stands;
      total_stands += body[i].bike_stands;

      if(body[i].available_bike_stands == 0) {
        full_stations += 1;
      }

      if(body[i].available_bikes == 0) {
        empty_stations += 1;
      }
    }

    // Create csv file if i doesnt exists
    initializeCSVFile(totalStatsFile, totalStatsHeaders);
    initializeCSVFile(stationsStatsFile, stationsStatsHeaders);

    var line = update_time  + ',' +
               avail_bikes  + ',' +
               avail_stands + ',' +
               total_stands + '\n';

    fs.appendFileSync(totalStatsFile, line, 'utf-8');

    line = update_time     + ',' +
           empty_stations  + ',' +
           full_stations   + '\n';

    fs.appendFileSync(stationsStatsFile, line, 'utf-8');
  });
}

function initializeCSVFile(filename, headers) {
  if(!fs.existsSync(filename)){
    f = fs.openSync(filename, 'w');
    fs.closeSync(f);
    fs.appendFileSync(filename,headers,'utf-8');
  }
}
