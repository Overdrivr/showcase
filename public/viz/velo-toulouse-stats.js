var width = 1000,
    height = 500;

var margin = {
  left: 50,
  top: 20
};

var chart = d3.select("svg")
    .attr("width", width + 2 * margin.left)
    .attr("height", height + 2 * margin.top)
      .append('g')
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var y = d3.scaleLinear()
          .range([height - margin.top, margin.top]);

var x = d3.scaleTime()
          .range([0, width]);

var yAxis = d3.axisLeft()
            .scale(y);

var xAxis = d3.axisTop()
            .scale(x)
            .ticks(d3.timeDay.every(1));

d3.csv("/data/polling_velo_toulouse.csv", function(error, data) {
  data.forEach(function(d, i){
    for(prop in d){
      if(d.hasOwnProperty(prop)){
        d[prop] = +d[prop];
      };
    };
  });

  var bikes = function(d){return d.available_bikes};
  var stands = function(d){return d.available_stands};
  var time = function(d){return d.unixtime};

  var ymin = Math.min(d3.min(data, bikes),d3.min(data,stands));
  var ymax = Math.max(d3.max(data, bikes),d3.max(data,stands));

  x.domain([new Date(d3.min(data, time)),  new Date(d3.max(data, time))]);
  y.domain([ymin, ymax]);

  // Plot data
  var bikes_curve = d3.line()
    .x(function(d, i) { return x(new Date(d.unixtime)); })
    .y(function(d, i) { return y(d.available_bikes); });

  var stands_curve = d3.line()
    .x(function(d, i) { return x(new Date(d.unixtime)); })
    .y(function(d, i) { return y(d.available_stands); });

  chart.append("path")
      .attr("d", bikes_curve(data))
      .attr("fill", "none")
      .attr("stroke", "blue")
      .attr("stroke-width", 2)
      .attr("fill", "none");

  chart.append("path")
      .attr("d", stands_curve(data))
      .attr("fill", "none")
      .attr("stroke", "green")
      .attr("stroke-width", 2)
      .attr("fill", "none");

  // Plot axes
  chart.append("g")
      .attr("class", "Count")
      .call(yAxis);

  chart.append("g")
      .attr("class", "Time")
      .call(xAxis)
      .attr("transform", "translate(0," + y(ymax/2 + ymin/2) + ")");

  // legend
  chart.append("g")
      .attr("class", "legend-bikes")
      .attr("transform", "translate(" + margin.left / 2 + "," + y(ymax/2 + ymin/2 - 1.5 * margin.top) + ")")
      .append("line")
        .attr("x1", 0)
        .attr("y1", 0)
        .attr("x2", 20)
        .attr("y2", 0)
        .attr("stroke","blue")
        .attr("stroke-width","3")

  chart.select(".legend-bikes")
      .append("text")
        .attr("x", 25)
        .attr("y", 5)
        .text("Velos disponibles")

  chart.append("g")
      .attr("class", "legend-stands")
      .attr("transform", "translate(" + margin.left / 2 + "," + y(ymax/2 + ymin/2 + 1.5 * margin.top) + ")")
      .append("line")
        .attr("x1", 0)
        .attr("y1", 0)
        .attr("x2", 20)
        .attr("y2", 0)
        .attr("stroke","green")
        .attr("stroke-width","3")

  chart.select(".legend-stands")
      .append("text")
        .attr("x", 25)
        .attr("y", 5)
        .text("Stations disponibles")

});
